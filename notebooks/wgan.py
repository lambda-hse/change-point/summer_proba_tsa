import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torch.autograd import Variable

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")



class Generator(nn.Module):
    
    def __init__(self, n_inputs, n_outputs):
        super(Generator, self).__init__()
        
        ### YOUR CODE IS HERE ######
        self.net = nn.Sequential(
                                nn.Linear(n_inputs, 100), 
                                #nn.BatchNorm1d(100),
                                nn.ReLU(),
                                nn.Linear(100, 100),
                                #nn.BatchNorm1d(100),
                                nn.ReLU(),
                                nn.Linear(100, n_outputs))
        ### THE END OF YOUR CODE ###

    def forward(self, z, y):
        zy = torch.cat((z, y), dim=1)
        return self.net(zy)
    
    

class Discriminator(nn.Module):
    
    def __init__(self, n_inputs):
        super(Discriminator, self).__init__()
        
        ### YOUR CODE IS HERE ######
        self.net = nn.Sequential(
                                nn.Linear(n_inputs, 100), 
                                nn.ReLU(),
                                nn.Linear(100, 100),
                                nn.ReLU(),
                                nn.Linear(100, 1))
        ### THE END OF YOUR CODE ###

    def forward(self, x, y):
        xy = torch.cat((x, y), dim=1)
        return self.net(xy)
    
    

class Fitter(object):
    
    def __init__(self, generator, discriminator, batch_size=32, n_epochs=10, latent_dim=1, lr=0.0001, n_critic=5):
        
        self.generator = generator
        self.discriminator = discriminator
        self.batch_size = batch_size
        self.n_epochs = n_epochs
        self.latent_dim = latent_dim
        self.lr = lr
        self.n_critic = n_critic
        
        self.opt_gen  = torch.optim.RMSprop(self.generator.parameters(), lr=self.lr)
        self.opt_disc = torch.optim.RMSprop(self.discriminator.parameters(), lr=self.lr)
        
        self.generator.to(DEVICE)
        self.discriminator.to(DEVICE)
    
    
    def fit(self, X, y):
        
        # numpy to tensor
        X_real = torch.tensor(X, dtype=torch.float, device=DEVICE)
        y_cond = torch.tensor(y, dtype=torch.float, device=DEVICE)
        
        # tensor to dataset
        dataset_real = TensorDataset(X_real, y_cond)
        
        # Turn on training
        self.generator.train(True)
        self.discriminator.train(True)
        
        self.loss_history = []

        # Fit GAN
        for epoch in range(self.n_epochs):
            for i, (real_batch, cond_batch) in enumerate(DataLoader(dataset_real, batch_size=self.batch_size, shuffle=True)):
                
                ### YOUR CODE IS HERE ######
                
                # generate a batch of fake observations
                z_noise = torch.normal(0, 1, (len(real_batch), self.latent_dim))
                fake_batch = self.generator(z_noise, cond_batch)
    
                ### Discriminator
                loss_disc = -torch.mean(self.discriminator(real_batch, cond_batch)) + torch.mean(self.discriminator(fake_batch, cond_batch))

                # optimization step
                self.opt_disc.zero_grad()
                loss_disc.backward(retain_graph=True)
                self.opt_disc.step()

                # Clip weights of discriminator
                for p in self.discriminator.parameters():
                    p.data.clamp_(-0.01, 0.01)
                    
                ### Generator
                if i % self.n_critic == 0:
                    
                    # measures generator's ability to fool the discriminator
                    loss_gen = torch.mean(self.discriminator(real_batch, cond_batch)) - torch.mean(self.discriminator(fake_batch, cond_batch))
                    
                    # optimization step
                    self.opt_gen.zero_grad()
                    loss_gen.backward()
                    self.opt_gen.step()
                    
                ### THE END OF YOUR CODE ###
                    
            # caiculate and store loss after an epoch
            Z_noise = torch.normal(0, 1, (len(X_real), self.latent_dim))
            X_fake = self.generator(Z_noise, y_cond)
            loss_epoch = torch.mean(self.discriminator(X_real, y_cond)) - torch.mean(self.discriminator(X_fake, y_cond))
            self.loss_history.append(loss_epoch.detach().cpu())
                    
        # Turn off training
        self.generator.train(False)
        self.discriminator.train(False)
        
        
    def predict(self, X):
        Z = torch.normal(0, 1, (len(X), self.latent_dim))
        X = torch.tensor(X, dtype=torch.float, device=DEVICE)
        y_pred = self.generator(Z, X).cpu().detach().numpy()
        return y_pred